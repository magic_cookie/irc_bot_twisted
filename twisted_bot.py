# -*- coding: utf-8 -*-
#!/usr/bin/python

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol, defer
from http_proxy_connect import *
import time

BOT_HOST = "irc.server.com"
BOT_PORT = 6667

BOT_NICKNAME = "bot"
BOT_PASSWORD = "password"
BOT_CHANNEL = "#channel"

BOT_PROXY = False
BOT_HTTP_PROXY_HOST = "proxy.server"
BOT_HTTP_PROXY_PORT = 8080

PREFIX = "someprefix:: " 

global users
users = {}

class Irc_Client(irc.IRCClient):

	def connectionMade(self):
		self.nickname = BOT_NICKNAME
		self.channel = BOT_CHANNEL
		irc.IRCClient.connectionMade(self)
		self.factory.instance = self

	def connectionLost(self, reason):
		irc.IRCClient.connectionLost(self, reason)

	def signedOn(self):
		self.msg('NickServ', 'IDENTIFY %s' % BOT_PASSWORD)
		self.join(self.channel)

	def joined(self, channel):
		pass

	def userLeft(self, user, channel):
		remove_user_from_list(user)

	def userQuit(self, user, quitMessage):
		remove_user_from_list(user)

	def userKicked(self, kickee, channel, kicker, message):
		remove_user_from_list(user)

	def irc_NICK(self, prefix, params):
		old_nick = prefix.split('!')[0].lower()
		remove_user_from_list(old_nick)

	def remove_user_from_list(self, nick):
		if nick in users:
			del users[nick]

	def privmsg(self, user, channel, msg):
		global users
		user = user.split('!', 1)[0]
		if msg.startswith("!") and channel.lower() == self.nickname.lower():
			if msg == "!test":
				self.msg(user, "this is some command")
		elif msg and channel.lower() == self.nickname.lower():
				if user in users.keys():
					if users[user]["status"]:
						self.send_stream(PREFIX+msg)
					else:
						self.check_new_user(user).addCallback(self.ok_go, PREFIX+msg, user)
				else:
					self.check_new_user(user).addCallback(self.ok_go, PREFIX+msg, user)

	def ok_go(self, confirmed, msg, user):
		if confirmed:
			self.send_stream(msg)
		else:
			self.msg(user, "Please, register")

	def check_new_user(self, user):
		global users
		d = defer.Deferred()
		users.update({ user : { "status" : False, "defer": d } })
		self.msg("NickServ", 'STATUS %s' % user)
		return d

	def send_stream(self, msg):
		self.msg(self.channel, msg)

	def noticed(self, user, channel, message):
		print "NOTICED", user, channel, message
		nickserv = user.split('!', 1)[0] == "NickServ"
		who = message.split(' ')[1].strip()
		authorized = message.split(' ')[2] == '3'
		if nickserv:
			if authorized:
				users[who]["status"] = True
				users[who]["defer"].callback(True)
			else:
				try:
					users[who]["defer"].callback(False)
				except:
					pass

	def receivedMOTD(self, motd):
		print "MOTD", motd

	def irc_unknown(self, prefix, command, params):
		print "UNKNOWN", prefix, command, params

	def irc_ERR_ERRONEUSNICKNAME(self, prefix, params):
		print "ERR_ERRONEUSNICKNAME", prefix, params

class Irc_Connect_Factory(protocol.ClientFactory):

	def __init__(self):
		self.instance = ''
		self.maxtries = 5
		self.tries = 0

	def buildProtocol(self, addr):
		self.p = Irc_Client()
		self.p.factory = self
		return self.p

	def clientConnectionLost(self, connector, reason):
		try:
			time.sleep(10)
			connector.connect()
			self.tries = 0
		except:
			self.tries += 1
			if self.tries == self.maxtries:
				reactor.stop()

	def clientConnectionFailed(self, connector, reason):
		print "IRC connection failed:", reason
		reactor.stop()

	def get_instance(self):
		return self.instance

def init_bot():
	factory = Irc_Connect_Factory()
	if BOT_PROXY:
		proxy = HTTPProxyConnector( BOT_HTTP_PROXY_HOST, BOT_HTTP_PROXY_PORT)
		proxy.connectTCP( BOT_HOST, BOT_PORT, factory )
	else:
		reactor.connectTCP( BOT_HOST, BOT_PORT, factory )
	reactor.run()

if __name__ == "__main__":
	init_bot()